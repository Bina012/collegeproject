@extends('layouts.backend')
@section('title',$panel.' create')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <h5 class="card-title">Create {{$panel}}</h5>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route'=>$base_route.'store','method'=>'post']) !!}
                            <div class="form-group">
                                {!! Form::label('name','Name') !!}
                                {!! Form::text('name',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('route','Route') !!}
                                {!! Form::text('route',null,['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('status','Status') !!}
                                {!! Form::radio('status',1) !!}Active
                                {!! Form::radio('status',0) !!}Deactive
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save '.$panel,['class'=>'btn btn-success']); !!}
                                {!! Form::reset('Clear '.$panel,['class'=>'btn btn-warning']) !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
