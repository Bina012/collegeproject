<?php
return[
    'site_name' => 'Ecommerce Site',
    'info' => 'Sign in to start your session',
    'remember' => 'Remember Me',
    'sign_in' => 'Sign In',
];
