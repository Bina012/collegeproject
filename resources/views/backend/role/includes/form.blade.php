<div class="form-group">
    {!! Form::label('name', 'Name'); !!}
    {!! Form::text('name', null,['class' => 'form-control']); !!}
    @include('backend.common.validation',['field' => 'name'])
</div>
<div class="form-group">
    {!! Form::label('key', 'Key'); !!}
    {!! Form::number('key',null,['class'=>'form-control']); !!}
    @include('backend.common.validation',['field' => 'key'])
</div>
<div class="form-group">
    {!! Form::label('status', 'Status'); !!}
    {!! Form::radio('status', 1); !!} Active
    {!! Form::radio('status', 0,true); !!} Deactive
    @include('backend.common.validation',['field' => 'status'])
</div>
<div class="form-group">
    {!! Form::submit('Save ' . $panel, ['class' => 'btn btn-info']); !!}
    {!! Form::reset('Clear', ['class' => 'btn btn-danger']); !!}
</div>
