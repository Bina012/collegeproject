<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\CategoryRequest;
use App\Http\Requests\SubcategoryRequest;
use App\Http\Requests\TagRequest;
use App\Models\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class TagController extends BackendBaseController
{
    protected $panel = 'Tag';
    protected $folder = 'backend.tag.';
    protected $base_route = 'backend.tag.';
    protected $file_path = '';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        $data['record'] = Tag::orderby('created_by','desc')->get();
        return view($this->__LoadDataToView($this->folder.'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->__LoadDataToView($this->folder.'create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(TagRequest $request)
    {
        $request->request->add(['created_by'=>Auth::user()->id]);
        $record = Tag::create($request->all());
        if ($record){
            $request->session()->flash('success',$this->panel.' Created successfully');
        }else{
            $request->session()->flash('error',$this->panel.' creation failed');
        }
        return redirect()->route($this->base_route.'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Tag::find($id);
        return view($this->__loadDataToView($this->base_route.'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['record'] = Tag::find($id);
        return view($this->__loadDataToView($this->base_route.'edit'),compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['record'] = Tag::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        if ($data['record']->update($request->all())){
            $request->session()->flash('success',$this->panel." Updated successfully");
        }else{
            $request->session()->flash('error',$this->panel." updation failed");
        }
        return redirect()->route($this->base_route.'index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data['record'] = Tag::find($id);
        if ($data['record']->delete()){
            $request->session()->flash('success',$this->panel. "delete success");
            return redirect()->route($this->base_route.'index');
        }else{
            $request->session()->flash('error',$this->panel. "delete failed");
            return redirect()->route($this->base_route.'index');
        }
    }
}
