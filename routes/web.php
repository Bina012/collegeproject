<?php
use Illuminate\Database\Eloquent\Model\Category;
use App\Http\Controllers\Backend\CategoryController;
use App\Http\Controllers\Backend\SubcategoryController;
use App\Http\Controllers\Backend\TagController;
use App\Http\Controllers\Backend\ProductController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Backend\UnitController;
use App\Http\Controllers\Backend\AttributeController;
use App\Http\Controllers\Backend\ModuleController;
use App\Http\Controllers\Backend\PermissionController;
use App\Http\Controllers\Backend\UserController;
use App\Http\Controllers\Backend\RoleController;
use App\Http\Controllers\Backend\CouponController;
use App\Http\Controllers\Backend\SettingController;
use App\Http\Controllers\Frontend\FrontendController;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/', [FrontendController::class, 'index'])->name('frontend.index');
Route::get('/category', [FrontendController::class, 'category'])->name('frontend.category');

Auth::routes();
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/category/getSubcategory', [App\Http\Controllers\Backend\CategoryController::class, 'getSubcategory'])->name('category.getSubcategory');

Route::prefix('backend')->name('backend.')->middleware(['auth'])->group(function(){
    Route::get('/role/assign_form/{id}',[RoleController::class,'assignForm'])->name('role.assign_form');
    Route::post('/role/assign_permission',[RoleController::class,'assignPermission'])->name('role.assign_permission');

    Route::resource('/category', CategoryController::class);
    Route::resource('/tag',TagController::class);
    Route::resource('/subcategory', SubcategoryController::class);
    Route::resource('/product', ProductController::class);
    Route::resource('/unit',UnitController::class);
    Route::resource('/attribute',AttributeController::class);
    Route::resource('/module',ModuleController::class);
    Route::resource('/permission',PermissionController::class);
    Route::resource('/user',UserController::class);
    Route::resource('/role',RoleController::class);
    Route::resource('/coupon',CouponController::class);
    Route::resource('/setting',SettingController::class);

});
