@extends('layouts.backend')
@section('title',$panel . ' Create')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{route('backend.coupon.index')}}" class="btn btn-info">List {{$panel}}</a>
                    <div class="card card-primary card-outline">
                        <div class="card-header">
                            <h3 class="card-title">Create {{$panel}}</h3>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => $base_route . 'store', 'method' => 'post','files' => true]) !!}
                            <div class="form-group">
                                {!! Form::label('title', 'Title'); !!}
                                {!! Form::text('title', null,['class' => 'form-control']); !!}
                                @include('backend.common.validation',['field' => 'title'])
                            </div>
                            <div class="form-group">
                                {!! Form::label('code', 'Code'); !!}
                                {!! Form::number('code', null,['class' => 'form-control']); !!}
                                @include('backend.common.validation',['field' => 'code'])
                            </div>
                            <div class="form-group">
                                {!! Form::label('discount_percentage', 'Discount Percentage'); !!}
                                {!! Form::number('discount_percentage', null,['class' => 'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('date_from', 'Date From'); !!}
                                {!! Form::number('date_from', null,['class' => 'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('date_to', 'Date To'); !!}
                                {!! Form::number('date_to', null,['class' => 'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('status', 'Status'); !!}
                                {!! Form::radio('status', 1); !!} Active
                                {!! Form::radio('status', 0,true); !!} Deactive
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save ' . $panel, ['class' => 'btn btn-info']); !!}
                                {!! Form::reset('Clear', ['class' => 'btn btn-danger']); !!}
                            </div>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
