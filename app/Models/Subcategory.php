<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    use HasFactory;
    protected $table = 'subcategories';
    protected $fillable = ['category_id','name','slug','status','rank','short_description','description','meta_description','image','meta_title','meta_description','meta_Keyword','created_by','updated_by'];
}
