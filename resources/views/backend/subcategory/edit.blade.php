@extends('layouts.backend')
@section('title',$panel . 'Details')
@section('main-content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            @include('backend.common.flash_message')
                            {!! Form::model($data['record'],['route'=>$base_route . 'update',$data['record']->id,'method'=>'put','files'=>'true']) !!}
                            <div class="form-group">
                                {!! Form::label('category_id','Category'); !!}
                                {!! Form::select('category_id',$data['categories'],null,['class'=>'form-control','placeholder'=> 'Select']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('name','Name'); !!}
                                {!! Form::text('name',null,['class'=>'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('slug','Slug');!!}
                                {!! Form::text('slug',null,['class'=>'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('rank','Rank'); !!}
                                {!! Form::number('rank',null,['class'=>'form-control']); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('short_description','Short Description'); !!}
                                {!! Form::textarea('short_description',null,['class'=>'form-control','rows'=>2]) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('description','Description'); !!}
                                {!! Form::textarea('description',null,['class'=>'form-control','rows'=>2]); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('image_file','Image'); !!}
                                {!! Form::file('image_file',['class'=>'form-control']) !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_Keyword','Meta Keyword'); !!}
                                {!! Form::textarea('meta_Keyword',null,['class'=>'form-control','rows'=>2]); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_description','Meta Description');!!}
                                {!! Form::textarea('meta_description',null,['class'=>'form-control','rows'=>2]); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::label('meta_title','Meta Title'); !!}
                                {!! Form::textarea('meta_title',null,['class'=>'form-control','rows'=>2]); !!}
                            </div>
                            <div class="form-group">
                                {!! Form::submit('Save ' .  $panel,['class'=>'btn btn-info']); !!}
                                {!! Form::reset('Clear ' .  $panel,['class'=>'btn btn-danger']); !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
