<div class="form-group">
    {!! Form::label('name','Name');!!}
    {!! Form::text('name',null,['class'=>'form-control']);!!}
    @include('backend.common.validation',['field'=>'name'])
</div>
<div class="form-group">
    {!! Form::label('slug','Slug'); !!}
    {!! Form::text('slug',null,['class'=>'form-control']); !!}
    @include('backend.common.validation',['field'=>'slug'])
</div>
<div class="form-group">
    {!! Form::label('rank','Rank'); !!}
    {!! Form::number('rank',null,['class'=>'form-control']); !!}
    @include('backend.common.validation',['field'=>'rank'])
</div>
<div class="form-group">
    {!! Form::label('short_description','Short Description') !!}
    {!! Form::textarea('short_description',null,['class'=>'form-control','rows'=>2]); !!}
    @include('backend.common.validation',['field'=>'short_description'])
</div>
<div class="form-group">
    {!! Form::label('description','Description'); !!}
    {!! Form::textarea('description',null,['class'=>'form-control','rows'=>2]); !!}
</div>
<div class="form-group">
    {!! Form::label('image_file','Image'); !!}
    {!! Form::file('image_file',['class'=>'form-control']); !!}
</div>
<div class="form-group">
    {!! Form::label('meta_title','Meta Title'); !!}
    {!! Form::text('meta_title',null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
    {!! Form::label('meta_keyword','Meta Keyword'); !!}
    {!! Form::text('meta_keyword',null,['class'=>'form-control']); !!}
</div>
<div class="form-group">
    {!! Form::label('meta_description','Meta Description'); !!}
    {!! Form::textarea('meta_description',null,['class'=>'form-control','rows'=>2]); !!}
</div>
<div class="form-group">
    {!! Form::label('status','Status'); !!}
    {!! Form::radio('status',1);!!}Active
    {!! Form::radio('status',0,true);!!}Deactive
</div>
<div class="form-group">
    {!! Form::submit($button. '  '.  $panel,['class'=>'btn btn-info']); !!}
    {!! Form::reset('Clear ' .  $panel,['class'=>'btn btn-danger']); !!}
</div>
