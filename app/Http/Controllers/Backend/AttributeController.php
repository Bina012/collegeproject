<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttributeRequest;
use App\Models\Attribute;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AttributeController extends BackendBaseController
{
    protected $panel = "Attribute";
    protected $folder = "backend.attribute.";
    protected $base_route = "backend.attribute.";
    protected $file_path = "";
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['record'] = Attribute::orderby('created_at','desc')->get();
        return view($this->__loadDataToView($this->base_route.'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view($this->__loadDataToView($this->base_route.'create'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AttributeRequest $request)
    {
        $request->request->add(['created_by' => Auth::user()->id]);
         $record = Attribute::create($request->all());
         if ($record){
             $request->session()->flash('success',$this->panel."Created successfully");
         }else{
             $request->session()->flash('error',$this->panel." creation failed");
         }
         return redirect()->route($this->base_route.'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Attribute::find($id);
        return view($this->__loadDataToView($this->base_route.'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['record'] = Attribute::find($id);
        if ($data['record']){
            return view($this->__loadDataToView($this->base_route.'index'),compact('data'));
        }else{
            request()->session()->flash('error',$this->panel."Invalid request");
            return redirect()->route($this->base_route.'index');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['record'] = Attribute::find($id);
        $request->request->add(['updated_by' => Auth::user()->id]);
        if ($data['record']->update($request->all())){
            $request->session()->flash('success',$this->panel."updated successfully");
        }else{
            $request->session()->flash('error',$this->panel." update failed");
        }
        return redirect()->route($this->base_route.'index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data['record'] = Attribute::find($id);
        if ($data['record']->delete()){
            $request->session()->flash('success',$this->panel.' deletion successfully');
            return redirect()->route($this->base_route.'index');
        }else{
            $request->session()->flash('error',$this->panel.' deletion failed');
            return redirect()->route($this->base_route.'index');
        }
    }
}
