@extends('layouts.backend')
@section('title',$panel,' List')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-6">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Slug</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                @foreach($data['record'] as $record)
                                    <tbody>
                                        <tr>
                                            <td>{{$loop->index+1}}</td>
                                            <td>{{$record->name}}</td>
                                            <td>{{$record->slug}}</td>
                                            <td>
                                                <a href="{{route('backend.tag.show',$record->id)}}" class="btn btn-info">View detail</a>
                                                <a href="{{route('backend.tag.edit',$record->id)}}" class="btn btn-warning">Edit</a>
                                                <form action="{{route('backend.tag.destroy',$record->id)}}" method="post">
                                                    <input type="hidden" name="_method" value="DELETE">
                                                    @csrf
                                                    <button type="submit" class="btn btn-danger">Delete</button>
                                                </form>
                                            </td>
                                        </tr>
                                    </tbody>
                                    @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
