@extends('layouts.backend')
@section('title',$panel.' edit')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <h5 class="card-title">Edit {{$panel}}</h5>
                        </div>
                        <div class="card-body">
                            {!! Form::model($data['record'],['route' => [$base_route.'update',$data['record']->id],'method'=>'put']) !!}
                            @include('backend.user.includes.form',['button' => 'Update'])
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
