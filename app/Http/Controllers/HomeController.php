<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Backend\BackendBaseController;
use Illuminate\Http\Request;

class HomeController extends BackendBaseController
{
    protected $panel ='Dashboard';
    protected $folder = 'backend.dashboard';
    protected $base_route = 'backend.dashboard.';
    protected $file_path = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view($this->__LoadDataToView('dashboard'));
    }
}
