<?php

namespace App\Http\Controllers\backend;

use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Models\Module;
use App\Models\Permission;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use phpDocumentor\Reflection\Types\AbstractList;

class PermissionController extends BackendBaseController
{
    protected $panel = 'Permission';
    protected $base_route = 'backend.permission.';
    protected $folder = 'backend.permission.';
    protected $file_path = '';
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['record'] = Permission::orderby('created_at','desc')->get();
        return view($this->__loadDataToView($this->base_route.'index'),compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['modules'] = Module::pluck('name','id');
        return view($this->__loadDataToView($this->base_route.'create'),compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PermissionRequest $request)
    {
        $request->request->add(['created_by'=>Auth::user()->id]);
        $record = Permission::create($request->all());
        if ($record){
            $request->session()->flash('success',$this->panel.'Created successfully');
        }else{
            $request->session()->flash('error',$this->panel.'creation failed');
        }
        return redirect()->route($this->base_route.'index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['record'] = Permission::find($id);
        return view($this->__loadDataToView($this->base_route.'show'),compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['modules'] = Module::pluck('name','id');
        $data['record'] = Permission::find($id);
        if ($data['record']){
            return view($this->__loadDataToView($this->base_route.'edit'),compact('data'));
        }
        else{
            request()->session()->flash('error','Invalid Request');
            return redirect()->route($this->base_route.'index');
        }


    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PermissionRequest $request, $id)
    {
        $data['record'] = Permission::find($id);
        $request->request->add(['updated_by'=>Auth::user()->id]);
        if ($data['record']->update($request->all())){
            $request->session()->flash('success',$this->panel."update success");
        }else{
            $request->session()->flash('error',$this->panel."updation failed");
        }
        return redirect()->route($this->base_route.'index');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request,$id)
    {
        $data['record'] = Permission::find($id);
        if ($data['record']->delete()){
            $request->session()->flash('success',$this->panel."delete success");
            return redirect()->route($this->base_route.'index');
        }else{
            $request->session()->flash('error',$this->panel."deletion failed");
            return redirect()->route($this->base_route.'index');
        }
    }
}
