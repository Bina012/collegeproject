@extends('layouts.backend')
@section('title',$panel.' list')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <a href="{{route('backend.module.create')}}" class="btn btn-info">Create {{$panel}}</a>
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <h5 class="card-title">{{$panel}} List</h5>
                        </div>
                        <div class="card-body">
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th>S.N</th>
                                        <th>Name</th>
                                        <th>Route</th>
                                        <td>Action</td>
                                    </tr>
                                </thead>
                                @foreach($data['record'] as $record)
                                    <tbody>
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td>{{$record->name}}</td>
                                        <td>{{$record->route}}</td>
                                        <td>
                                            <a href="{{route('backend.module.show',$record->id)}}" class="btn btn-info">View Details</a>
                                            <a href="{{route('backend.module.edit',$record->id)}}" class="btn btn-warning">Edit</a>
                                            <form action="{{route('backend.module.destroy',$record->id)}}" method="post">
                                                <input type="hidden" name="_method" value="DELETE">
                                                @csrf
                                                <button type="submit" class="btn btn-danger">Delete</button>
                                            </form>
                                        </td>
                                    </tr>
                                    </tbody>
                                @endforeach
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
