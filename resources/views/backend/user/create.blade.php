@extends('layouts.backend')
@section('title',$panel.' create')
@section('main-content')
    <!-- Main content -->
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card card-primary card-outline">
                        <div class="card-body">
                            <h5 class="card-title">Create {{$panel}}</h5>
                        </div>
                        <div class="card-body">
                            {!! Form::open(['route' => $base_route.'store','method'=>'post']) !!}
                                @include('backend.user.includes.form',['button' => 'Save'])
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
